package com.sdok.nensovanchananra;

import android.net.Uri;

import java.net.URL;

public class Products {
    private  String Product_name;
    private  String Product_price;
    private  String Description;
    private URL ImageUrl;
    private String Category_name;

    public String getProduct_name() {
        return Product_name;
    }

    public void setProduct_name(String product_name) {
        Product_name = product_name;
    }

    public String getProduct_price() {
        return Product_price;
    }

    public void setProduct_price(String product_price) {
        Product_price = product_price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public URL getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(URL imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getCategory_name() {
        return Category_name;
    }

    public void setCategory_name(String category_name) {
        Category_name = category_name;
    }

    public Products(String product_name, String product_price, String description, URL imageUrl, String category_name) {
        Product_name = product_name;
        Product_price = product_price;
        Description = description;
        ImageUrl = imageUrl;
        Category_name = category_name;
    }
}
