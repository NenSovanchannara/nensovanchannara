package com.sdok.nensovanchananra;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Homerecycleradapter extends RecyclerView.Adapter<Homerecycleradapter.Viewholder> {
    List<String> data;

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //return UI item

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleritem,parent,false);

        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        //Connect UI action
        holder.product_image.setImageResource(Integer.parseInt(data.get(position)));
        holder.product_name.setText(data.get(position));
        holder.product_price.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class Viewholder extends RecyclerView.ViewHolder{
        //declare view and find id
        TextView product_name;
        TextView product_price;
        ImageView product_image;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            product_image=itemView.findViewById(R.id.product_pic);
            product_name = itemView.findViewById(R.id.product_name);
            product_price=itemView.findViewById(R.id.product_price);
        }
    }


}
