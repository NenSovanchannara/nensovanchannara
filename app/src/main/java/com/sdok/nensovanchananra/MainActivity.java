package com.sdok.nensovanchananra;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNav=findViewById(R.id.menu);
        bottomNav.setOnNavigationItemSelectedListener(navlistener);
        getSupportFragmentManager().beginTransaction().replace(R.id.container_fragment,new Homefragment()).commit();
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navlistener=
            new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectFragment=null;
            switch (item.getItemId()){
                case R.id.nav_home:
                    selectFragment=new Homefragment();
                    break;
                case R.id.nav_categories:
                    selectFragment=new Catecgoriesfragment();
                    break;
                case R.id.nav_search:
                    selectFragment=new Searchfragment();
                    break;
                case R.id.nav_account:
                    selectFragment=new Accountfragment();
                    break;

            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container_fragment, selectFragment).commit();
            return true;
        }
    };
}
